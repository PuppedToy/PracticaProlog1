% Practica de la vibora: Alejandro Alcázar Sacristán, Adrián Ciudad Sanz, Guillermo de Lima Rodríguez
alumno_prode('Alcazar', 'Sacristan', 'Alejandro', 'w140113').
alumno_prode('Ciudad', 'Sanz', 'Adrian', 'w140292').
alumno_prode('De Lima', 'Rodriguez', 'Guillermo', 'w140091').

vibora(X, Y, [_Z|Zs], [R|Rs]) :-
	vibora_(X, X, Y, Y, Zs, R, Rs, par).

vibora_(X, Xg, [], Yg, [_Z|Zs], [], [R|Rs], par) :-
	vibora_(X, Xg, Yg, Yg, Zs, Rr, Rs, impar),
	rev(Rr, R).

vibora_(X, Xg, [], Yg, [_Z|Zs], [], [R|Rs], impar) :-
	vibora_(X, Xg, Yg, Yg, Zs, R, Rs, par).

vibora_([X|Xs], Xg, [_Y|Ys], Yg, Z, [X|Rs], Rg, Paridad) :-
	vibora_(Xs, Xg, Ys, Yg, Z, Rs, Rg, Paridad).

vibora_([], [Xg|Xs], Y, Yg, Z, R, Rg, Paridad) :-
	vibora_([Xg|Xs], [Xg|Xs], Y, Yg, Z, R, Rg, Paridad). % Si se acaba la secuencia de anillos se reinicia

vibora_(_X, _Xg, [], _Yg, [], [], [], _Paridad). % En este caso hemos terminado

rev(X,Y) :- rev_(X,[],Y).
rev_([X|Xs],A,Y) :- rev_(Xs,[X|A],Y).
rev_([],A,A).

% vibora([a,b,c,d], [_,_,_,_,_], [_,_,_], R).

% vibora([a,b,c,d], [_,_,_,_,_], [_,_,_], [[a,b,c,d,a],[b,a,d,c,b],[c,d,a,b,c]]).

% vibora(R, [_,_,_,_,_], [_,_,_], [[a,b,c,d,a],[b,a,d,c,b],[c,d,a,b,c]]).